####################Kubernetes Configmap######################

locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH


apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.demo-node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH
}

output "config_map_aws_auth" {
  value = "${local.config_map_aws_auth}"
}


locals {
  kubeconfig = <<KUBECONFIG

###################Kubernetes configuration file Kubeconfig################

apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.demo.endpoint}
    certificate-authority-data: ${aws_eks_cluster.demo.certificate_authority.0.data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${var.cluster-name}"
KUBECONFIG
}

#########
# Outputs
#########


output "kubeconfig" {
  value = "${local.kubeconfig}"
}

output "endpoint_rds"{
	value = "${aws_rds_cluster.aurora.endpoint}"
}

output "engine"{
	value = "${aws_rds_cluster.aurora.engine}"
}

output "reader_endpoint"{
	value = "${aws_rds_cluster.aurora.reader_endpoint}"
}

/*output "maintenance_window" {
	value = "${aws_rds_cluster.aurora.maintenance_window}"
}*/

output "database_name" {
	value = "${aws_rds_cluster.aurora.database_name}"
}

output "db_port" {
	value = "${aws_rds_cluster.aurora.port}"
}

output "master_username" {
	value = "${aws_rds_cluster.aurora.master_username}"
}

output "storage_encrypted" {
	value = "${aws_rds_cluster.aurora.storage_encrypted}"
}


output "backup_retention_period" {
	value = "${aws_rds_cluster.aurora.backup_retention_period}"
}



######Elastic cache endpoint######

/*output "cache_endpoint"{
	value = "${aws_elasticache_replication_group.elastic_cache_poc_rg.primary_endpoint_address}"
}*/


output "configuration_endpoint"{
	value = "${aws_elasticache_replication_group.elastic_cache_poc_rg.configuration_endpoint_address}"
}

output "member_clusters"{
	value = "${aws_elasticache_replication_group.elastic_cache_poc_rg.member_clusters}"
}

/*output "primary_endpoint_address"{
	value = "${aws_elasticache_replication_group.elastic_cache_poc_rg.primary_endpoint_address}"
}*/


#######EC2############
output "public_dns_reprocessing" {
	value = "${aws_instance.Infra-Poc.public_dns}"
}

output "public_ip_reprocessing" {
	value = "${aws_instance.Infra-Poc.public_ip}"
}


#########EKS##########

output "eks_version" {
	value = "${aws_eks_cluster.demo.version}"
}




