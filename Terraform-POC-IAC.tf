###########
#Networking
###########

resource "aws_vpc" "ra-poc-1-vpc"{
	cidr_block = "${var.vpc_cidr}"
	instance_tenancy = "default"
	enable_dns_support = true
	enable_dns_hostnames = true
	tags{
		Name = "ra-poc-1-vpc"
	}
}


resource "aws_subnet" "Public-subnet"{
	availability_zone = "${data.aws_availability_zones.azs.names[0]}"
	vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"
    	cidr_block = "${var.public_subnet_cidr}"
	map_public_ip_on_launch = true

    	tags {
		
   		"kubernetes.io/cluster/ra-poc-1-eks-cluster" = "shared"
    		"Name"	= "Zone-A-public-subnet"
  	}
	
}


resource "aws_subnet" "Public-subnet-1"{
	availability_zone = "${data.aws_availability_zones.azs.names[1]}"
	vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"
    	cidr_block = "${var.public_subnet_cidr_1}"
	map_public_ip_on_launch = true

    	tags {
		
   		"kubernetes.io/cluster/ra-poc-1-eks-cluster"                 = "shared"
    		"Name"	= "Zone-B-public-subnet"
  	}	
	
	
}

resource "aws_subnet" "Public-subnet-2"{
	availability_zone = "${data.aws_availability_zones.azs.names[2]}"
	vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"
    	cidr_block = "${var.public_subnet_cidr_2}"
	map_public_ip_on_launch = true

    	tags {
		
   		"kubernetes.io/cluster/ra-poc-1-eks-cluster"                 = "shared"
		"Name"	= "Zone-C-public-subnet"
    		
  	}
}

resource "aws_internet_gateway" "inet-gateway" {
    vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"

    tags {
        Name = "Inet-Gateway"
    }
	depends_on = ["aws_vpc.ra-poc-1-vpc"]
}

resource "aws_route_table" "r_t" {
    vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.inet-gateway.id}"
    }

	tags {
		Name = "Route_table_Public_Subnet"
	}
}


resource "aws_route_table_association" "r_t_a_zone_a" {
  subnet_id      = "${aws_subnet.Public-subnet.id}"
  route_table_id = "${aws_route_table.r_t.id}"
}

resource "aws_route_table_association" "r_t_a_zone_b" {
  subnet_id      = "${aws_subnet.Public-subnet-1.id}"
  route_table_id = "${aws_route_table.r_t.id}"
}

resource "aws_route_table_association" "r_t_a_zone_c" {
  subnet_id      = "${aws_subnet.Public-subnet-2.id}"
  route_table_id = "${aws_route_table.r_t.id}"
}


####################
#RDS-DB Subnet Group
####################

resource "aws_db_subnet_group" "aurora" {
  name       = "aurora_subnet"
  description= "The subnets used to form aurora-cluster"
  subnet_ids = ["${aws_subnet.Public-subnet.id}", "${aws_subnet.Public-subnet-1.id}", "${aws_subnet.Public-subnet-2.id}"]


  tags {
    Name = "Aurora subnet group"
  }
}


###########################
#RDS-Aurora-Security Groups
###########################

resource "aws_security_group" "aurora" {
  name = "Aurora-MySQL"
  description = "Allows requests for Aurora-DB application"
  vpc_id      = "${aws_vpc.ra-poc-1-vpc.id}"
  ingress {
      from_port = 3306
      to_port = 3306
      protocol = "tcp"
      cidr_blocks = ["xx.12.45.102/32", "xx.201.137.178/32", "${aws_instance.Infra-Poc.public_ip}/32"]
      description = "Aurora-MySQL"
	}
      tags{
	Name = "Security group for Aurora-DB-Server "
	}
	
	depends_on = ["aws_instance.Infra-Poc"]
}

###################
#RDS-Aurora-Cluster
###################

resource "aws_rds_cluster" "aurora" {
  cluster_identifier      = "ra-poc-1-aurora"
  availability_zones      = ["${data.aws_availability_zones.azs.names}"]
  db_subnet_group_name    = "${aws_db_subnet_group.aurora.name}"
  engine        	  = "aurora-mysql"
  engine_mode             = "provisioned"
  database_name           = "aurora"
  master_username         = "auroraTest"
  master_password         = "xxxxxxxxx"
  backup_retention_period = 1
  storage_encrypted       = true
  skip_final_snapshot     = true
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  deletion_protection     = false
  backtrack_window        = 0
  vpc_security_group_ids  = ["${aws_security_group.aurora.id}"]
  apply_immediately       = true
  preferred_backup_window = "10:30-11:00"
  tags{
	Name = "Aurora-Cluster"
 }
	depends_on = ["aws_instance.Infra-Poc"]
}

################################
#RDS-Aurora-Cluster-DB-Instances
################################

resource "aws_rds_cluster_instance" "cluster_instance" {
  cluster_identifier   = "${aws_rds_cluster.aurora.id}"
  identifier	       = "ra-poc-1-aurora-1"
  instance_class       = "db.t2.medium"
  engine               = "aurora-mysql"
  publicly_accessible  = true
  db_subnet_group_name = "${aws_db_subnet_group.aurora.name}"
  availability_zone    = "${data.aws_availability_zones.azs.names[0]}"
  apply_immediately    = true
  promotion_tier       = 0
  tags{
	Name = "Aurora cluster db instance Primary"
 }
}

resource "aws_rds_cluster_instance" "cluster_instance-3" {
  cluster_identifier   = "${aws_rds_cluster.aurora.id}"
  identifier	       = "ra-poc-1-aurora-2"
  instance_class       = "db.t2.medium"
  engine               = "aurora-mysql"
  publicly_accessible  = true
  db_subnet_group_name = "${aws_db_subnet_group.aurora.name}"
  availability_zone    = "${data.aws_availability_zones.azs.names[1]}"
  apply_immediately    = true
  promotion_tier       = 1
  tags{
	Name = "Aurora cluster db instance"
 }
}


######################
#EC-Redis-Subnet Group
######################

resource "aws_elasticache_subnet_group" "cache_subnet_group" {
  name       = "ra-poc-1-elasticache"
  subnet_ids = ["${aws_subnet.Public-subnet.id}", "${aws_subnet.Public-subnet-1.id}", "${aws_subnet.Public-subnet-2.id}"]
}


########################
#EC-Redis-Security Group
########################


resource "aws_security_group" "redis" {
  name = "redis-sg"
  description = "ElastiCache Redis Securtity Group"
    vpc_id = "${aws_vpc.ra-poc-1-vpc.id}"

    ingress {
      from_port = "6379"
      to_port = "6379"
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0", "${aws_instance.Infra-Poc.public_ip}/32"]
    }

    egress {
      from_port = "0"
      to_port = "0"
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
      Name = "ElastiCache Redis Node"
    }
	
	depends_on = ["aws_instance.Infra-Poc"]
}


###########################
#EC-Redis-Replication Group
###########################

resource "aws_elasticache_replication_group" "elastic_cache_poc_rg" {
  replication_group_id          = "ra-poc-1-elasticache"
  replication_group_description = "ra-poc-1-vpc"
  node_type                     = "cache.t2.medium"
  engine			            = "redis"
  port                          = 6379
  at_rest_encryption_enabled    = false
  transit_encryption_enabled    = false
  #auth_token			= "thisis0neunbreakab1e"
  automatic_failover_enabled    = true
  auto_minor_version_upgrade    = true
  #availability_zones            = ["${data.aws_availability_zones.azs.names}"]
  security_group_ids     	= ["${aws_security_group.redis.id}"]
  subnet_group_name		= "${aws_elasticache_subnet_group.cache_subnet_group.name}"
  snapshot_window		= "01:00-02:30"
  snapshot_retention_limit	= 2
  maintenance_window		= "sun:10:30-sun:11:30"
  apply_immediately		= true
  

  cluster_mode {
    replicas_per_node_group = 1
    num_node_groups         = 3
  }
  tags{
	Name = "Elastic cache POC"
	}
	
	depends_on = ["aws_instance.Infra-Poc"]
}

###############################################
#EKS-Master-Cluster-IAM-Role
###############################################


resource "aws_iam_role" "demo-cluster" {
  name = "ra-poc-1-eks-master"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "demo-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.demo-cluster.name}"
}

resource "aws_iam_role_policy_attachment" "demo-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.demo-cluster.name}"
}

####################
#EKS-Worker-IAM-Role
####################


resource "aws_iam_role" "demo-node" {
  name = "ra-poc-1-eks-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_instance_profile" "demo-node" {
  name = "terraform-eks"
  role = "${aws_iam_role.demo-node.name}"
}

###################
#EKS-Cluster-Master
###################

resource "aws_eks_cluster" "demo" {
  name            = "${var.cluster-name}"
  role_arn        = "${aws_iam_role.demo-cluster.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.demo-cluster.id}"]
    subnet_ids         = ["${aws_subnet.Public-subnet.id}", "${aws_subnet.Public-subnet-1.id}", "${aws_subnet.Public-subnet-2.id}"] 
  }

  depends_on = [
    "aws_iam_role_policy_attachment.demo-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.demo-cluster-AmazonEKSServicePolicy",
  ]
}


###########################
#EKS and EC2 Security Group
###########################


#####################Security group EKS Cluster################
resource "aws_security_group" "demo-cluster" {
  name        = "terraform-eks-poc-demo-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.ra-poc-1-vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "terraform-eks-poc-demo"
  }
}

# OPTIONAL: Allow inbound traffic from your local workstation external IP
#           to the Kubernetes.
resource "aws_security_group_rule" "demo-cluster-ingress-workstation-https" {
  cidr_blocks       = ["xx.201.137.178/32", "xx.12.45.102/32"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.demo-cluster.id}"
  to_port           = 443
  type              = "ingress"
}

##################WORKER NODE SECURITY GROUPS######################

resource "aws_security_group" "demo-node" {
  name        = "terraform-eks-poc-demo-node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${aws_vpc.ra-poc-1-vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "terraform-eks-poc-demo-node",
     "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "demo-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.demo-node.id}"
  source_security_group_id = "${aws_security_group.demo-node.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "demo-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.demo-node.id}"
  source_security_group_id = "${aws_security_group.demo-cluster.id}"
  to_port                  = 65535
  type                     = "ingress"
}

############For Cluster#############
resource "aws_security_group_rule" "demo-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.demo-cluster.id}"
  source_security_group_id = "${aws_security_group.demo-node.id}"
  to_port                  = 443
  type                     = "ingress"
}



###############EC2##################

resource "aws_security_group" "infra-poc-sg" {
  name = "Infra-POC-EC2"
  description = "Allows requests from Devolopers"
  vpc_id      = "${aws_vpc.ra-poc-1-vpc.id}"
  ingress {
      from_port = 8084
      to_port = 8084
      protocol = "tcp"
      cidr_blocks = ["xx.12.45.102/32","xx.201.137.178/32"]
      description = "WebService"
  }
  ingress {
    from_port = 12345
    to_port = 12345
    protocol = "tcp"
    cidr_blocks = ["xx.12.45.102/32","xx.201.137.178/32"]
    description = "Mongo Database"
  }
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["xx.12.45.102/32","xx.201.137.178/32"]
      description = "SSH for Teezle"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
	tags{
	Name = "Security group for Re-Processing"
	}
}

resource "aws_security_group" "infra-poc-db-sg" {
  name = "Infra-POC-EC2-DB"
  description = "Allows requests from Devolopers"
  vpc_id      = "${aws_vpc.ra-poc-1-vpc.id}"
  ingress {
      from_port = 12345
      to_port = 12345
      protocol = "tcp"
      cidr_blocks = ["xx.12.45.102/32", "xx.201.137.178/32", "${aws_instance.Infra-Poc.public_ip}/32"]
  }
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["xx.12.45.102/32", "xx.201.137.178/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
	tags{
	Name = "Security group for DB-Server"
	}
}

resource "aws_security_group_rule" "db-server-sg" {
  description              = "Local VPC"
  from_port                = 12345
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.infra-poc-db-sg.id}"
cidr_blocks		           = ["${var.vpc_cidr}"]
  to_port                  = 12345
  type                     = "ingress"
}

#################################
#EKS-Launch Configuration and ASG
#################################

#############LAUNCH CONFIGURATION###############

locals {
  demo-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.demo.endpoint}' --b64-cluster-ca '${aws_eks_cluster.demo.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "demo" {
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.demo-node.name}"
  image_id                    = "${var.eks_ami_ohio}"
  instance_type               = "t2.large"
  key_name		      = "${aws_key_pair.infra-poc.key_name}"
  name_prefix                 = "terraform-eks-demo"
  security_groups             = ["${aws_security_group.demo-node.id}"]
  user_data_base64            = "${base64encode(local.demo-node-userdata)}"
  
  lifecycle {
    create_before_destroy = true
  }
}


###############AUTO SCALING GROUP################

resource "aws_autoscaling_group" "demo" {
  desired_capacity     = 2
  launch_configuration = "${aws_launch_configuration.demo.id}"
  max_size             = 2
  min_size             = 1
  name                 = "ra-poc-1-eks"
  vpc_zone_identifier  = ["${aws_subnet.Public-subnet.id}", "${aws_subnet.Public-subnet-1.id}", "${aws_subnet.Public-subnet-2.id}"]

  tag {
    key                 = "Name"
    value               = "ra-poc-1-eks-node"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}

