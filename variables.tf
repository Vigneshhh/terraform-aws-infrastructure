variable cluster-name{
	default = "ra-poc-1-eks-cluster"
}


provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.region}"
}


variable "aws_access_key"{
	type = "string"
	default = "xxxxxxxxxxxxxx"
}

variable "aws_secret_key"{
	type = "string"
	default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

variable "vpc_cidr"{
	type = "string"
	default = "10.0.0.0/16"
}

variable "public_subnet_cidr"{
	type = "string"
	default = "10.0.1.0/24"
}

variable "public_subnet_cidr_1"{
	type = "string"
	default = "10.0.2.0/24"
}

variable "public_subnet_cidr_2"{
	type = "string"
	default = "10.0.3.0/24"
}

data "aws_availability_zones" "azs" {}

variable "project_a_key"{
	#type = "string"
	default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFWsebI8/kr1o5GLyq+OG8pQT3HfvUdqH6Gfvmn0/CLD/nwqeIFTebdM7J0GNlrMc3drSVQPIetpH6MwBCybo8ly1txz5q1oo0RavbplpW8zLmrRlBftamVYaRZXXBAFG/wMK5xluPDuKVSIakcxHwuNlQbW7DDsPJ1C6urKuKOGN0YzxtZpTU+JTalDiYD77Pveur2dOYBTWl3GL8YLqtqz0wyBvq2W58HHrRlIUDH3bukIsJ27UQoPeiEfVGPEvz8FWraOc3m638bG1LO+5FZM3gEh4sHDuqrTMfI+NWy/CQrEO9v/6uGpX4iKw49pspqHi/VrpSa3d58yfDdEoj root"
}

variable "project_a_key_nat"{
	#type = "string"
	default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFWsebI8/kr1o5GLyq+OG8pQT3HfvUdqH6Gfvmn0/CLD/nwqeIFTebdM7J0GNlrMc3drSVQPIetpH6MwBCybo8ly1txz5q1oo0RavbplpW8zLmrRlBftamVYaRZXXBAFG/wMK5xluPDuKVSIakcxHwuNlQbW7DDsPJ1C6urKuKOGN0YzxtZpTU+JTalDiYD77Pveur2dOYBTWl3GL8YLqtqz0wyBvq2W58HHrRlIUDH3bukIsJ27UQoPeiEfVGPEvz8FWraOc3m638bG1LO+5FZM3gEh4sHDuqrTMfI+NWy/CQrEO9v/6uGpX4iKw49pspqHi/VrpSa3d58yfDdEoj root"
}

resource "aws_key_pair" "infra-poc"{
  key_name   = "infra-poc"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCZeQmMVdLE/S/CVh4B1E88UCRhj4UJXNApYf8cAINgy8Blc74XQZKHOt1GT3zZxBhT4X1MjVFeDHlrZ5SVCmWeocmxLo8vP1xYUKnk6QlTwnnWuA8Md/fLXcLbMK9iIPpM+RDIckKwbY4/MMR2P0HWb72bN+r+TZdx/HB5Lxhdu6HnOWq3JQzLvt0/3ejWqi9g4I6VZrBlSHxVWEaTyHS1XelgohXA08FsqiCTqHgmZwctu06x6SaSwQ4MLMfDzrp3qg3u5szlZZbY7g+rRON0zfvrawR4A8pzBjcTn+n9jspmqTYey6naXitX4rsVCRZiPkrBfN6vKOH6Mop+KjuP ubuntu"
}


variable "region"{
	default = "us-east-2"
}

variable "ubuntu_18_04"{
	default = "ami-0427e8367e3770df1"
}

variable "ubuntu_18_04_oregon" {
	default = "ami-0bbe6b35405ecebdb"
}

variable "ubuntu_18_04_ohio" {
	default = "ami-0653e888ec96eab9b"
}


variable "nat_ami"{
	default = "ami-1ec1407a"
}


/*data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-v*"]
  }

  most_recent = true
  owners      = ["self"] # Amazon EKS AMI Account ID
}*/

data "aws_ami" "eks-worker" {
most_recent = true
  filter {
    name   = "name"
    values = ["amazon-eks-*"]
   }
}


variable "eks_ami_singapore"{
	default = "ami-0549ac6995b998478"
	}

variable "eks_ami_oregon" {
	default = "ami-0a2abab4107669c1b"
}

variable "eks_ami_ohio" {
	default = "ami-0c2e8d28b1f854c68"
}


